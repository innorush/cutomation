#!/usr/bin/env python3

import argparse
import sys
from collections import namedtuple
from math import ceil

class Range(namedtuple('Range', ('start', 'end'))):

    def __lt__(self, other):
        return self.end <= other.start

# values in the range [n*a, n*a + b] are snapped to n*a, while
# values in the range (n*a + b, (n+1)*a] are snapped to (n+1)*a
def snap(x, a, b):
    return a * ceil((x - b) / a)

class RangeSet:
    def __init__(self, ranges=None):
        self.ranges = []
        if ranges:
            for r in ranges:
                self.append(Range(*r))

    def isempty(self):
        return len(self.ranges) == 0

    def append(self, range):
        assert type(range) is Range
        assert self.isempty() or self.ranges[-1] < range
        self.ranges.append(range)

    def toboundarylist(self, invert=False):
        result=[]
        s = -1 if invert==True else 1
        for r in self.ranges:
            result.append((r.start, 1*s))
            result.append((r.end, -1*s))
        return result

    def shrink(self, x):
        result=RangeSet()
        for r in self.ranges:
            s = r.start + x
            e = r.end - x
            if s < e:
                result.append(Range(s, e))
        return result

    def snap_rangestart(self, a, b):
        result=RangeSet()
        for r in self.ranges:
            s = snap(r.start, a, b)
            e = r.end
            if s < e:
                result.append(Range(s, e))
        return result

    def snap_rangeend(self, a, b):
        result=RangeSet()
        for r in self.ranges:
            s = r.start
            e = snap(r.end, a, b)
            if s < e:
                result.append(Range(s, e))
        return result

    def diff(self, other):
        b = self.toboundarylist()
        b.extend(other.toboundarylist(invert=True))
        b.sort()
        result = RangeSet()
        insideness = 0
        range_start = None
        for x in b:
            if insideness == 0 and x[1] == 1:
                assert range_start is None
                range_start = x[0]
            elif insideness == 1 and x[1] == -1:
                assert range_start is not None
                result.append(Range(range_start, x[0]))
                range_start = None
            insideness += x[1]
        return result


    @staticmethod
    def load(filename):
        rs = RangeSet()
        with open(filename, 'r') as f:
            for line in f:
                start,end=line.split(',')
                rs.append(Range(float(start), float(end)))
        return rs

    def __str__(self):
        return "\n".join('{},{}'.format(*r) for r in self.ranges)


if __name__ == '__main__':
    def die(msg):
        print('ERROR:', msg, file=sys.stderr)
        sys.exit(1)

    parser = argparse.ArgumentParser(description='')
    op = parser.add_mutually_exclusive_group()
    op.add_argument('--diff', action='store_true')
    op.add_argument('--shrink', type=float)
    op.add_argument('--snap-rangestart', type=float, nargs=2)
    op.add_argument('--snap-rangeend', type=float, nargs=2)
    parser.add_argument('rangesets', nargs='+')

    args = parser.parse_args()

    rs1 = RangeSet.load(args.rangesets[0])
    if args.diff:
        if len(args.rangesets) != 2:
            die('diff operation requires 2 rangesets')
        rs2 = RangeSet.load(args.rangesets[1])
        result = rs1.diff(rs2)
    elif args.shrink is not None:
        result = rs1.shrink(args.shrink)
    elif args.snap_rangestart is not None:
        result = rs1.snap_rangestart(*args.snap_rangestart)
    elif args.snap_rangeend is not None:
        result = rs1.snap_rangeend(*args.snap_rangeend)
    else:
        die("invalid operation '{}'".format(args.operation))

    print(result)
