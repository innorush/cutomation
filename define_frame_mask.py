#!/usr/bin/env python3

import argparse
import cv2
import numpy as np

parser = argparse.ArgumentParser(description='')
parser.add_argument('frame_file')

args = parser.parse_args()

def drawRectsInPlace(img, rects, color):
    for rect in rects:
        cv2.rectangle(img, *rect, color, -1)

def drawRects(img, rects, color, opacity):
    result = img.copy()
    drawRectsInPlace(result, rects, color)
    cv2.addWeighted(img, 1 - opacity, result, opacity, 0, result)
    return result

class MainWindow:
    def __init__(self, image_file_path):
        self.img_path = image_file_path
        self.orig_image = cv2.imread(self.img_path)
        self.edited_image = self.orig_image
        self.selected_rects = []
        self.winname = 'MainWindow'
        cv2.namedWindow(self.winname)
        cv2.setMouseCallback(self.winname, self.onmouse)
        self.drag_start = None

    def show(self):
        cv2.imshow(self.winname, self.orig_image)
        while self.isOpen():
            k = cv2.waitKey(33)
            if k != -1:
                a = k % 256
                if a == 27: # Esc key to stop
                    break
                elif a == ord('u'):
                    self.dropLastRect()
                elif a == ord('s'):
                    self.save()

    def addRect(self, rect):
        self.selected_rects.append(rect)
        self.update()

    def dropLastRect(self):
        if self.selected_rects:
            self.selected_rects.pop()
            self.update()

    def save(self):
        b, g, r = cv2.split(self.orig_image)
        a = np.zeros(b.shape, dtype=b.dtype)
        drawRectsInPlace(a, self.selected_rects, 255)
        img_to_save = cv2.merge((b,g,r,a))
        cv2.imwrite(self.img_path, img_to_save)

    def updateView(self, pending_selection_rect=None):
        WHITE = (255, 255, 255)
        if pending_selection_rect is not None:
            img = drawRects(self.edited_image, [pending_selection_rect], WHITE, 0.2)
        else:
            img = self.edited_image
        cv2.imshow(self.winname, img)

    def update(self):
        BLUE = (255, 128, 128)
        self.edited_image = drawRects(self.orig_image, self.selected_rects, BLUE, 0.4)
        self.updateView()

    def drag_rect(self, x, y):
        xo, yo = self.drag_start
        x0, y0 = np.minimum([xo, yo], [x, y])
        x1, y1 = np.maximum([xo, yo], [x, y])
        if x1-x0 > 0 and y1-y0 > 0:
            return (x0, y0), (x1, y1)
        else:
            return None

    def onmouse(self, event, x, y, flags, param):
        x, y = np.int16([x, y]) # BUG
        if event == cv2.EVENT_LBUTTONDOWN:
            self.drag_start = (x, y)
        elif event == cv2.EVENT_LBUTTONUP:
            if self.drag_start:
                rect = self.drag_rect(x, y)
                self.drag_start = None
                if rect:
                    self.addRect(rect)
        elif self.drag_start:
            rect = self.drag_rect(x, y)
            if rect:
                self.updateView(rect)

    def isOpen(self):
        return cv2.getWindowProperty(self.winname, cv2.WND_PROP_VISIBLE) >= 1


#def askSaveFilename():
#    from tkinter import Tk, filedialog
#
#    filetypes = (("PNG files","*.png"),)
#    root = Tk()
#    root.withdraw()
#    filepath =  filedialog.asksaveasfilename(title = "Save frame",
#                                             filetypes = filetypes)
#    root.destroy()


w = MainWindow(args.frame_file)
w.show()

