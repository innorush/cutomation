#!/usr/bin/env python3

import argparse
from bisect import bisect
import functools
import os
import subprocess
import sys
import time
import zmq

def Range(s):
    start,end = s.split(',')
    start,end = float(start), float(end)
    if not start < end:
        raise ValueError
    return start, end

parser = argparse.ArgumentParser(description='')
parser.add_argument('--debug', action='store_true')
parser.add_argument('--start-at', type=float, default=0.0)
parser.add_argument('recording')
parser.add_argument('ranges', metavar='range', nargs='+', type=Range)

cmdline = parser.parse_args()

end_of_recording = object()

class TimeStamps(list):
    def append(self, t):
        assert len(self) == 0 or self[-1] < t
        super().append(t)

    def check_time(self, t):
        p = bisect(self, t)
        return p % 2, self[p] if p < len(self) else end_of_recording

ts = TimeStamps()

for r in cmdline.ranges:
    ts.append(r[0])
    ts.append(r[1])

def update_textfile(action, t):
    if t is end_of_recording:
        text = r'Will {} till the end of video'.format(action)
    else:
        text = r'Will {} next %{{eif:ceil({}-t):d}} seconds'.format(action, t)
    with open('preview.text.tmp', 'w') as f:
        print(text, file=f, end='')
    os.rename('preview.text.tmp', 'preview.text')

update_textfile('remove', 0)

fullbox='drawbox=color=white@0.7:x=0:y=0:w=1280:h=720:t=360'
textbox='drawbox=color=white@0.7:x=280:y=340:w=750:h=80:t=40'
text="drawtext=x=300:y=360:fontcolor=black:fontsize=50:textfile=preview.text:reload=1"
videopipeline=','.join([fullbox, textbox, 'zmq', text])

ffplay = subprocess.Popen(['ffplay', '-hide_banner',
                                     '-autoexit',
                                     '-ss', str(cmdline.start_at),
                                     '-i', cmdline.recording,
                                     '-vf', videopipeline],
                           stderr=subprocess.PIPE,
                           universal_newlines=True)

def is_stats_line(fields):
    return (len(fields) == 12   and
            fields[1] == 'A-V:' and
            fields[3] == 'fd='  and
            fields[5] == 'aq='  and
            fields[7] == 'vq='  and
            fields[9] == 'sq=')

zcontext = zmq.Context()
ffplay_zaddr = 'tcp://localhost:5555'

def log(*args, **kwargs):
    if cmdline.debug:
        print(*args, file=sys.stderr, flush=True, **kwargs)

def connect_to_ffplay():
    global z
    log('z.socket')
    z = zcontext.socket(zmq.REQ)
    z.RCVTIMEO = 100
    log('z.connect: ', end='')
    z.connect(ffplay_zaddr)
    log('connected')

connect_to_ffplay()

def try_zsend(msg):
    global z
    log('zmq.send:', msg)
    z.send(msg)
    reply = None
    try:
        log('zmq.recv: ', end='')
        reply = z.recv()
        log(reply)
        return True
    except zmq.error.ZMQError as e:
        log(e)
        log('z.close')
        z.close(linger=True)
        connect_to_ffplay()
        return False

def zsend(msg):
    try_zsend(msg) or try_zsend(msg)

keep = None
nextt = None
t = 0.0
with ffplay as proc:
    for line in proc.stderr:
        fields = line.strip().split()
        if is_stats_line(fields) and fields[0] != 'nan':
            prevt = t
            t = float(fields[0])
            print('t = {}\r'.format(t), end='')
            k,nt = ts.check_time(t)
            if nt != nextt or abs(t - prevt) > 1:
                keep,nextt = k,nt
                zsend('Parsed_drawbox_0 enable {}'.format(1-keep).encode())
                zsend('Parsed_drawbox_1 enable {}'.format(keep).encode())
                update_textfile('keep' if keep else 'remove', nextt)
