#!/usr/bin/env python3

import sys
import argparse
import cv2
import numpy as np

parser = argparse.ArgumentParser(description='')
parser.add_argument('--start-time', type=float, required=True)
parser.add_argument('--end-time', type=float, required=True)
parser.add_argument('--min-similarity', type=float, default=0.98)
parser.add_argument('--debug', action='store_true')
parser.add_argument('--progress', action='store_true')
parser.add_argument('video_file')
parser.add_argument('image')

args = parser.parse_args()


def read_frames(input_file, start_time, end_time):
    v = cv2.VideoCapture(args.video_file)
    v.set(cv2.CAP_PROP_POS_MSEC, 1000*start_time)

    while True:
        status,frame = v.read()
        if not status:
            return

        t = v.get(cv2.CAP_PROP_POS_MSEC) / 1000
        if t > args.end_time:
            return

        yield frame, t


class ReferenceFrame:
    def __init__(self, image_file_path):
        image = cv2.imread(image_file_path, cv2.IMREAD_UNCHANGED)
        assert image.shape[2] == 4
        self.mask = image[:,:,3] != 0
        image = image[:,:,:3]
        self.subimage = image[self.mask].astype(dtype='int16')
        mask_size, channel_count = self.subimage.shape
        self.max_possible_diff = 255.0 * mask_size * channel_count

    def similarity(self, other_frame):
        diff = abs(self.subimage - other_frame[self.mask])
        return 1.0 - diff.sum() / self.max_possible_diff


reframe = ReferenceFrame(args.image)

if args.debug:
    BLACK = (0, 0, 0)
    WHITE = (255,255,255)
    cv2.namedWindow('Frame')
    def display_frame(frame, t, s):
        cv2.rectangle(frame, (0, 0), (200, 100), WHITE, -1)
        cv2.addText(frame, "T = {:.2f}".format(t), (10, 30), "", 24, BLACK, 5)
        cv2.addText(frame, "s = {:.2f}".format(s), (10, 60), "", 24, BLACK, 5)
        cv2.imshow('Frame', frame)
        while cv2.getWindowProperty('Frame', cv2.WND_PROP_VISIBLE) > 0:
            k = cv2.waitKey(33)
            if k != -1:
                a = k % 256
                if a == 27: # Esc key to stop
                    sys.exit(1)
                elif a == ord('n'):
                    break


for frame,t in read_frames(args.video_file, args.start_time, args.end_time):
    s = reframe.similarity(frame)
    if args.progress:
        print('\rt = {:8.2f}, s = {:5.2f}   '.format(t, s), end='')
    if s > args.min_similarity:
        print('Found matching frame at {}'.format(t))
        if not args.debug:
            sys.exit(0)

    if args.debug:
        display_frame(frame, t, s)

sys.exit(1)
