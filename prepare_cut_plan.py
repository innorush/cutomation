#!/usr/bin/env python3

import argparse
import bisect
from decimal import Decimal
import os
import sys
import tempfile

def die(msg):
    print('ERROR:', msg, file=sys.stderr)
    sys.exit(1)

def parse_range(s):
    start, end = s.split(',')
    return Decimal(start), Decimal(end)

def read_frame_index(frame_index_file):
    with open(frame_index_file, 'r') as f:
        if f.readline().strip() != 'dts_time,pkt_flags':
            die("'{}' is not a valid frame index file".format(frame_index_file))

        result = []
        for l,line in enumerate(f):
            dts_time_str, pkt_flags = line.strip().split(',')
            dts_time = Decimal(dts_time_str)
            is_key_frame = 'K' in pkt_flags
            result.append((dts_time, is_key_frame))
    return result


parser = argparse.ArgumentParser(description='')
parser.add_argument('frame_index_file')
parser.add_argument('time_range', nargs='+')

args = parser.parse_args()

frame_index = read_frame_index(args.frame_index_file)

reencode = 'reencode'
extract = 'extract'
def video_segment_inclusion_plan(start, end):
    result = []

    s = bisect.bisect_left(frame_index, (start, False))
    e = bisect.bisect_left(frame_index, (end, False))
    sts = frame_index[s][0] if s < len(frame_index) else start
    ets = frame_index[e][0] if e < len(frame_index) else end

    k1 = s
    while k1 != e and not frame_index[k1][1]:
        k1 += 1
    k1ts = frame_index[k1][0]

    if k1 == e:
        result.append((reencode, sts, ets, e - s))
    else:
        if k1 != s:
            result.append((reencode, sts, k1ts, k1 - s))

        k2 = e
        k2ts = end
        if k2 != len(frame_index):
            while k2 != k1 and not frame_index[k2][1]:
                k2 -= 1
            k2ts = frame_index[k2][0]

        if k1 != k2:
            result.append((extract, k1ts, k2ts, k2-k1))

        if k2 != e:
            result.append((reencode, k2ts, ets, e-k2))

    return result

time_ranges = [parse_range(x) for x in args.time_range]
for start, end in time_ranges:
    vsip = video_segment_inclusion_plan(start, end)
    for x in vsip:
        print('video', *x)
    audio_start = vsip[0][1]
    audio_end = vsip[-1][2]
    print('audio extract', audio_start, audio_end)
