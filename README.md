# About this project

This project provides a set of utilities to enable (semi-)automatic cutting of
video recordings using simple recipes.